<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Lines Of Text</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" crossorigin="anonymous"></script>
    -->

<script>
</script>

  </head>
  <body>
    <form action="update_db.php" method="get">
    <label for="text">A Line Of Text:</label>
    <input type="text" class="form-control" id="text" name="text" autofocus>
    <input type="submit" class="btn btn-primary btn-block" value="Submit">
    </form>
    <hr>
    <div id="main" class="container">
      <pre>
<?php include("text_db.php");?>
      </pre>
    </div>

  </body>
</html>

